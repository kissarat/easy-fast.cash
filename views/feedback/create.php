<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\article\models\Article;
use app\widgets\Ext;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = Yii::t('app', 'Ticket');
?>
<div class="feedback-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
