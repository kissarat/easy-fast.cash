<?php
/** @var \app\models\Feedback $topic */
use app\helpers\God;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $topic->subject;
$this->params['breadcrumbs'][] = ['label' => 'Тикеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$format = God::format();
$manager = !Yii::$app->user->getIsGuest() && Yii::$app->user->can('manage');

$items = [];
/** @var \app\models\Message[] $messages */
foreach($messages as $message) {
    if (!$message->feedback) {
        throw new Exception(json_encode($message->attributes));
    }
    $cells = [];
    if ($message->user_id) {
        $cells[] = Html::tag('strong', $message->user_id);
    }
    $cells[] = Html::tag('i', $format->asDatetime($message->time));
    $cells[] = Html::tag('span', !Yii::$app->user->getIsGuest() && Yii::$app->user->can('manage')
        ? $message->ip : '');
    $cells[] = Html::tag('div', $message->content);
    $images = [];
    foreach($message->getImagesUrl() as $url) {
        $images[] = Html::img($url, [
            'class' => 'thumbnail'
        ]);
    }
    $cells[] = Html::tag('div', implode("\n", $images), ['class' => 'preview']);
    $items[] = Html::tag('li', implode("\n", $cells),
        ['class' => $message->answer ? 'answer' : 'question']);
}
?>
<div class="feedback-ticket">
    <h3>Тикет: <?= $topic->ticket ?></h3>
    <p>
    <?php
    if ($manager) {
        echo Html::a('Удалить', ['delete', 'id' => $topic->id], [
            'class' => 'btn btn-danger',
            'data-method' => 'post'
        ]);
    }
    ?>

    <?= Html::a('Закрыть тикет', ['close', 'ticket' => $topic->ticket], [
        'class' => 'btn btn-danger',
        'data-method' => 'post'
    ]) ?>
    </p>
    <?= Html::tag('p', $topic->email ? $topic->email : 'Пользователь не указал свой email') ?>
    <ul>
        <?= implode("\n", $items) ?>
    </ul>
    <?php
    /** @var \app\models\Message $new */
    if ($new) {
        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
        echo implode("\n", [
            $form->field($new, 'content')->textarea(),
            $form->field($new, 'images[]')->fileInput(['multiple' => true]),
            Html::submitButton('Отправить', ['class' => 'btn btn-primary'])
        ]);
        ActiveForm::end();
    }
    ?>
</div>
