<?php
/**
 * @link http://zenothing.com/
 */

use app\models\Feedback;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Feedback */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title  = Yii::t('app','Feedback');
?>
<div class="feedback-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create ticket'), ['create'], ['class' => 'btn btn-success']); ?>
    <form id="ticket">
        <input type="text" size="24" maxlength="24" />
        <?= Html::button(Yii::t('app', 'Show ticket')) ?>
    </form>
    </p>

    <?php
    if (Yii::$app->user->getIsGuest()) {
        if (isset($_COOKIE['ticket'])) {
            echo Html::a(Yii::t('app', 'Last ticket'), ['ticket', 'ticket' => $_COOKIE['ticket']]);
        }
    }
    else {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['class' => 'id']
                ],
                [
                    'attribute' => 'id',
                    'label' => Yii::t('app', 'User'),
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        $first = $model->getFirst();
                        return $first->user_id ? Html::a($first->user_id) : null;
                    }
                ],
                [
                    'attribute' => 'email',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->email) {
                            return Html::a($model->email, 'mailto:' . $model->email);
                        } else {
                            return Yii::t('app', 'registered');
                        }
                    }
                ],

                'subject:ntext',

                [
                    'label' => Yii::t('app', 'Action'),
                    'format' => 'html',
                    'value' => function (Feedback $model) {
                        return Html::a(Yii::t('app', 'Show'), ['ticket', 'ticket' => $model->ticket]);
                    }
                ]
            ],
        ]);
    }
    ?>

</div>
