<?php
use app\helpers\MainAsset;
use app\models\Article;
use app\models\Config;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<div id="background"></div>
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->id;
    echo Html::script("var user = '$user'");
}
echo "<div id='login'>";
if (Yii::$app->user->getIsGuest()) {
   echo Html::a(Yii::t('app','Login'), '/user/login',['id'=>'lg']);
   echo Html::a(Yii::t('app','Signup'), '/user/create',['id'=>'sn']);
}
echo "</div><div id='locale'>";
echo Html::a('EN', '/lang/lang/choice?code=en',['id'=>'en']);
echo Html::a('RU', '/lang/lang/choice?code=ru',['id'=>'ru']);
echo "</div>";
?>

<div class="wrap <?= $login ?>">
    <header>
        <h1><img class="logo" src="/images/logo.png"></h1>

        <?php
        NavBar::begin();

        $items = [
            ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index']],
            ['label' => Yii::t('app', 'Marketing'), 'url' => ['/article/view', 'id' => 'marketing']],
            ['label' => Yii::t('app', 'Agreement'), 'url' => ['/article/view', 'id' => 'agreement']],
            ['label' => Yii::t('app', 'FAQ'), 'url' => ['/article/view', 'id' => 'faq']],
            ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/index']],
            ['label' => Yii::t('app', 'Reviews'), 'url' => ['/review/review/index']],
        ];

        if (!Yii::$app->user->getIsGuest()) {
            $items[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout']];
            $items[] = ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/view', 'id' => Yii::$app->user->id]];
        }
            if (Yii::$app->user->can('manage')) {
                $items[] = ['label' => Yii::t('app', 'Investments'), 'url' => ['/pyramid/node/index']];
                $items[] = ['label' => Yii::t('app', 'Users'), 'url' => ['/user/index']];
               // $items[] = ['label' => Yii::t('app', 'Generate'), 'url' => ['/test/generate/index']];
                $items[] = ['label'=>Yii::t('app','Pillow'), 'url'=>['/pyramid/node/pillow']];
                $items[] = ['label'=>Yii::t('app','Invoice'), 'url'=>['/invoice/index']];
            }


        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => $items
        ]);
        NavBar::end();
        ?>
    </header>
    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= Alert::widget() ?>

        <?php
        if ('article' != Yii::$app->controller->id) {
            echo Article::widget(Yii::$app->request->getPathInfo());
        }
        ?>

        <?= $content ?>
    </div>
    <footer>
       <a href="http://selfiecam.in.ua/" target="_blank"><img src="/images/webChef_logo.png"></a>
    </footer>
</div>

<?php $this->endBody() ?>

<?php if (Config::get('common', 'heart')): ?>
    <script>
        (function(){
            var widget_id = <?= Config::get('common', 'heart') ?>;
            _shcp =[{widget_id : widget_id}];
            var lang =(navigator.language || navigator.systemLanguage
            || navigator.userLanguage ||"en")
                .substr(0,2).toLowerCase();
            var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
            var hcc = document.createElement("script");
            hcc.type ="text/javascript";
            hcc.async =true;
            hcc.src =("https:"== document.location.protocol ?"https":"http")
                +"://"+ url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>

<?php if (Config::get('common', 'google')): ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= Config::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>
</body>
</html>
<?php $this->endPage() ?>
