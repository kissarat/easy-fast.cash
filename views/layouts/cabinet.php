<?php
use app\models\Article;
use app\widgets\Menu;
use yii\helpers\Html;

$this->beginContent('@app/views/layouts/main.php');

$user = Yii::$app->user;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cabinet'), 'match' => ['/user/view', 'id' => $user->id]];

$items = [
    [
        'match' => '/^user/',
        'header' => Yii::t('app', 'Cabinet'),
        'content' => [
            Yii::t('app', 'View') => ['/user/view', 'id' => $user->id],
            Yii::t('app', 'Update') => ['/user/update', 'id' => $user->id],
            Yii::t('app', 'Password') => ['/user/password', 'id' => $user->id],
            Yii::t('app', 'Logout') => ['/user/logout', 'id' => $user->id],
        ]
    ],
    [
        'match' => '/^matrix/',
        'header' => Yii::t('app', 'Tariffs'),
        'content' => [
            Yii::t('app', 'Opened') => ['/pyramid/node/index', 'user_id' => $user->id],
            Yii::t('app', 'Buy') => ['/pyramid/node/create', 'user_id' => $user->id],
        ]
    ],
    [
        'match' => '/^invoice/',
        'header' => Yii::t('app', 'Invoices'),
        'content' => [
            Yii::t('app', 'Payment') => ['/invoice/payment', 'user_id' => $user->id],
            Yii::t('app', 'History') => ['/invoice/index', 'user_id' => $user->id],
            Yii::t('app', 'Withdraw') => ['/invoice/withdraw', 'user_id' => $user->id],
        ]
    ]
];
?>
<div class="row" id="cabinet">
    <div class="col-sm-2" id="menu">
        <?= Menu::widget(['items' => $items]) ?>
    </div>
    <div class="col-sm-10" id="content">
        <?= Article::widget(Yii::$app->request->getPathInfo()) ?>

        <?= $content ?>
    </div>
</div>
<?php
$this->endContent();
