<?php
/** @var \app\models\Article $model */

use yii\helpers\Html;
if ($model) {
    echo Html::beginTag('div', ['class' => 'article-view present', 'id' => $model->id]);
    if ($meta) {
        $this->title = $model->title;
        foreach ($model->getMetaTags() as $name => $content) {
            $this->registerMetaTag([
                'name' => $name,
                'content' => $content
            ]);
        }
    }

    echo Html::tag('h1', $model->title);
    if (Yii::$app->user->can('manage')) {
        echo Html::a(Yii::t('app', 'Edit text'), ['/article/edit', 'id' => $model->id, 'lang' => $model->lang]);
    }
    if ($model->content) {
        echo Html::tag('div', $model->content, ['class' => 'content']);
    }
}
else {
    echo Html::beginTag('div', ['class' => 'article-view absent', 'id' => $condition['id']]);
    $condition[0] = '/article/edit';
    if (Yii::$app->user->can('manage')) {
        echo Html::a(Yii::t('app', 'Edit text'), $condition);
    }
}

echo Html::endTag('div');
echo Html::tag('div', '', ['class' => 'article-bottom']);
