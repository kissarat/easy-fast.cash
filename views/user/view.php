<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use omnilight\assets\MomentAsset;
use omnilight\assets\MomentLanguageAsset;
use yii\helpers\Html;
use yii\widgets\DetailView;

MomentAsset::register($this);
MomentLanguageAsset::register($this);

$this->title = $model->id;

function spaces() {
    return implode(' ', func_get_args());
}

if (Yii::$app->user->can('manage')) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
}

$time = strtotime($model->time);
?>
<div class="user-view">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Password'), ['password', 'id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Invoices'), ['/invoice/index', 'user_id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Investments'), ['/pyramid/node/index', 'user_id' => $model->id]) ?>
    </p>

    <div class="attributes">
        <?= Html::tag('h2', $model->id) ?>
        <?= Html::tag('h3', spaces($model->forename, $model->surname)) ?>

        <?=Yii::t('app','Deductions')?> <?= Html::tag('span', date('Y-m-d H:i', $time), [
            'id' => 'last',
            'data-time' => $time,
            'data-left' => $time + 3600 * 24 * 7,
        ]) ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'account',
                    'value' => (float) $model->account
                ],
                [
                    'attribute' => 'referral_id',
                    'format' => 'html',
                    'value' => Html::a($model->referral_id,
                        ['view', 'referral_id' => $model->referral_id],
                        ['class' => 'value'])
                ],
                'email',
            ]
        ]) ?>
    </div>
</div>
