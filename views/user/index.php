<?php

use app\helpers\SQL;
use app\models\User;
use app\modules\pyramid\models\Node;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var \yii\data\ActiveDataProvider $dataProvider */
$this->title = Yii::t('app','Users');
?>
<div class="user-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['/admin/user/create']) ?>
    </p>

    <?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions'=>function($data){
            $node = new Node();
            $node = $node->find()->where(['user_id'=>$data->id, 'number'=>null])->one();
            if($node){
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'html',
                'value' => function(User $model) {
                    return Html::a($model->id, ['view', 'id' => $model->id]);
                }
            ],
            'surname',
            'forename',
            'email',
            'account',
            'perfect_usd',
            [
                'attribute' => 'referral_id',
                'format' => 'html',
                'value' => function(User $model) {
                    return Html::a($model->referral_id, ['index', 'referral_id' => $model->referral_id]);
                }
            ],
            [
//                'label' => Yii::t('app', 'Actions'),
//                'attribute' => 'id',
                'format' => 'html',
                'value' => function(User $model) {
                    $items = [
                        Html::a(' ', ['update', 'id' => $model->id], [
                            'class' => 'fa fa-pencil-square-o',
                            'title' => Yii::t('app', 'Edit')
                        ])
                    ];
                    if ($model->auth) {
                        $items[] = Html::a('', ['/admin/user/login', 'auth' => $model->auth], [
                            'class' => 'fa fa-sign-in',
                            'title' => Yii::t('app', 'Login')
                        ]);
                    }
                    $node = new Node();
                    $node = $node->find()->where(['user_id'=>$model->id, 'number'=>null])->one();
                    if($node){
                        $items[] = Html::a('', ['/user/unblock', 'user_id'=>$model->id], [
                            'class' => 'glyphicon glyphicon-expand',
                            'title' => Yii::t('app', 'Unblock')
                        ]);
                    }
                    return implode(' ', $items);
                }
            ],
        ]

    ]) ?>
</div>
