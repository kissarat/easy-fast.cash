<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use app\models\User;
use app\widgets\Fake;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($model->hasErrors()) {
    echo Html::script('var errors = ' . json_encode($model->getErrors()));
}

?>
<div class="user-edit">
    <?php
    $form = ActiveForm::begin([
        'options' => ['name' => 'User']
    ]);
    ?>

    <p>
        <?= Fake::widget() ?>
    </p>
    <div>
        <?php if(Yii::$app->controller->action->id == "create") {
            echo $form->field($model, 'referral_id');
            echo $form->field($model, 'id');
        }?>
        <?= $form->field($model, 'email') ?>
        <?php if(Yii::$app->controller->action->id == "create") {
            echo $form->field($model, 'account');
            echo $form->field($model, 'perfect_usd');
        }?>
        <?= $form->field($model, 'surname') ?>
        <?= $form->field($model, 'forename') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'repeat')->passwordInput() ?>
    </div>

    <p>
        <?php if(Yii::$app->controller->action->id == "create") { ?>
        <input type="checkbox" id="agree" />
        <label for="agree">я согласен с условиями компании</label>
        <?php } ?>
    </p>

    <p>
        <?= Html::submitButton($this->title, ['class' => 'btn btn-primary']) ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>
