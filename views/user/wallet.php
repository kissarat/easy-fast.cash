<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$users_url = ['label' => Yii::t('app', 'Users')];
if (Yii::$app->user->can('manage')) {
    $users_url['url'] = ['index'];
}
$this->params['breadcrumbs'][] = $users_url;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->id, 'url' => ['view', 'id' => Yii::$app->user->id]];

/** @var \app\models\User $model */
?>
<div class="user-wallet">
    <?php $form = ActiveForm::begin(); ?>

    <?php
    foreach($model->walletAttributes() as $name) {
        echo $form->field($model, $name);
    }
    ?>

    <?= Html::submitButton(Yii::t('app', 'Save')) ?>

    <?php ActiveForm::end(); ?>
</div>
