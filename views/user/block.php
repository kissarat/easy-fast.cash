<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 27.01.16
 * Time: 18:16
 */
$this->title = 'Account blocked';
?>
<div class="alert alert-danger" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Error:</span>
    <?=Yii::t('app','Profile blocked') ?>, <a href="/feedback/create/"><?=Yii::t('app','contact admin')?></a>.
</div>
