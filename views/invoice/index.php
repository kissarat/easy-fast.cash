<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
use yii\grid\GridView;
use yii\helpers\Html;
$this->title = Yii::t('app','Invoice');
?>
<div class="invoice-index">
    <p>
        <?= Html::a(Yii::t('app', 'Payment'), ['payment']) ?>
        <?= Html::a(Yii::t('app', 'Withdraw'), ['withdraw']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions'=>function($data){
            if(!$data->withdraw){
                return ['class'=>'info'];
            } else {
                if($data->status == 'success'){
                    return ['class'=>'success'];
                }else if($data->status == 'create'){
                    return ['class'=>'danger'];
                }
            }
        },
        'columns'=>[
            'user_id',
            'amount'=>[
                'attribute'=>'amount',
                'format'=>'raw',
                'value'=>function($data){
                    if(!$data->withdraw){
                        return $data->amount;
                    }
                    return $data->amount - $data->amount*0.03;
                }
            ],
            'status'=>[
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($data){
                    if (!$data->withdraw || $data->status == 'success') {
                        return Yii::t('app',$data->status);
                    }
                    if(!Yii::$app->user->can('manage') && $data->status == 'create'){
                        return Yii::t('app','waiting for admin confirm');
                    }
                    return Html::a(Yii::t('app','confirm output'),['success','id'=>$data->id]);
                }
            ],
            'wallet',
        ]
    ]) ?>
</div>
