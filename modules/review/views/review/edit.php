<?php

use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\modules\review\models\Review $model
 */

$this->title = Yii::t('app', $model->isNewRecord ? 'Create' : 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin();
if ('manage' == $model->scenario) {
    echo $form->field($model, 'user_id')->widget(AjaxComplete::className(), [
        'route' => ['/user/complete']
    ]);
}
echo $form->field($model, 'content')->textarea();

echo Html::submitButton(Yii::t('app', $this->title), ['class' => 'button']);

ActiveForm::end();
