<?php

use app\widgets\Menu;
use yii\helpers\Html;

/**
 * @var \app\modules\review\models\Review[] $models
 */
$this->title = Yii::t('app','Reviews');
?>
<div class="review-index">
    <p class="buttons">
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'button']) ?>
    </p>
    <dl>
        <?php foreach($models as $model): ?>
            <dt>
                <?= Html::a($model->user_id, ['user/view', 'name' => $model->user_id]) ?>
                <?php
                if (Yii::$app->user->can('manage')) {
                    $items = [
                        [
                            'url' => ['update', 'id' => $model->id],
                            'options' => [
                                'class' => 'fa fa-pencil',
                                'title' => Yii::t('app', 'Update')
                            ]
                        ],
                        [
                            'url' => ['delete', 'id' => $model->id],
                            'options' => [
                                'class' => 'fa fa-trash',
                                'title' => Yii::t('app', 'Delete')
                            ]
                        ],
                    ];

                    if (!$model->approved) {
                        array_unshift($items, [
                            'url' => ['approve', 'id' => $model->id],
                            'options' => [
                                'class' => 'fa fa-check-circle approve',
                                'title' => Yii::t('app', 'Approve')
                            ]
                        ]);
                    }

                    echo Menu::menu($items);
                }
                ?>
            </dt>
            <dd>
                <?= $model->content ?>
            </dd>
        <?php endforeach; ?>
    </dl>
</div>
