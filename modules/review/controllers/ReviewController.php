<?php

namespace app\modules\review\controllers;

use app\modules\review\models\Review;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class ReviewController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'approve'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['update', 'delete', 'approve'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ]
            ]
        ];
    }

    public function actionIndex($id = null) {
        $query = Review::find()->orderBy(['id' => SORT_DESC]);
        if (!Yii::$app->user->can('manage')) {
            $query->where('approved');
        }
        return $this->render('index', [
            'models' => $query->all(),
            'id' => $id
        ]);
    }

    public function actionCreate() {
        $model = new Review();
        if (Yii::$app->user->can('manage')) {
            $model->scenario = 'manage';
        }
        return $this->edit($model);
    }

    public function actionUpdate($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $model->approved = true;
        if ($model->save()) {
            Yii::t('app', 'Approved');
        }
        return $this->redirect(['index']);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted'));
        return $this->redirect(['index']);
    }

    protected function edit(Review $model) {
        if ('default' == $model->scenario) {
            $model->user_id = Yii::$app->user->id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var Review $model */
        if (($model = Review::findOne($id)) !== null) {
            if (Yii::$app->user->can('manage')) {
                $model->scenario = 'manage';
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
