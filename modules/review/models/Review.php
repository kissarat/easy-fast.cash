<?php

namespace app\modules\review\models;


use app\models\UserRecord;
use Yii;

/**
 * Class Review
 * @property integer $id
 * @property string $user_id
 * @property boolean $approved
 * @property string $content
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Review extends UserRecord {

    public function rules() {
        return [
            ['content', 'required'],
            ['content', 'string', 'min' => 20, 'max' => 4000],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['content'],
            'manage' => ['user_id', 'approved', 'content']
        ];
    }

    public function attributeLabels() {
        return [
            'user_id' => Yii::t('app', 'Login'),
            'content' => Yii::t('app', 'Content')
        ];
    }

    public function __toString() {
        return substr($this->content, 0, 60);
    }
}
