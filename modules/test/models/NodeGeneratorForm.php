<?php

namespace app\modules\test\models;


use app\models\User;
use app\modules\pyramid\models\Type;
use Faker\Factory;
use Yii;
use yii\base\Model;

class NodeGeneratorForm extends Model
{
    public $type_id;
    public $count;

    public function rules() {
        return [
            [['count', 'type_id'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'count' => Yii::t('app', 'Count'),
            'type_id' => Yii::t('app', 'Tariff'),
        ];
    }

    public function generate() {
        $factory = Factory::create();
        $models = [];
        if (empty($this->count)) {
            $this->count = User::find()->count() * Type::find()->count();
        }
        for($i = 0; $i < $this->count; $i++) {
//            try {
                $model = new NodeFake([
                    'type_id' => $this->type_id
                ]);
                $model->fake($factory);
                $model->open();
                $models[] = $model;
//            }
//            catch(\Exception $ex) {}
        }
        return $models;
    }
}
