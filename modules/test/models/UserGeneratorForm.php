<?php

namespace app\modules\test\models;


use app\helpers\SQL;
use Faker\Factory;
use yii\base\Model;

class UserGeneratorForm extends Model
{
    public $referral_id;
    public $prefix;
    public $count;

    public function rules()
    {
        return [
            ['count', 'required'],
            [['referral_id', 'prefix'], 'string'],
            ['count', 'integer'],
        ];
    }

    public function generate() {
        $initial = [];
        if (!empty($this->referral_id)) {
            $initial['referral_id'] = $this->referral_id;
        }
        if (empty($this->prefix)) {
            $this->prefix = 'user';
        }
        $prefix_max = SQL::queryCell('SELECT id FROM "user" WHERE id like :id ORDER BY id DESC LIMIT 1', [
           ':id' => $this->prefix . '%'
        ]);
        $number = 1;
        if ($prefix_max && preg_match('/(\d+)$/', $prefix_max, $postfix)) {
            $number = (int) $postfix[1];
        }

        $factory = Factory::create();
        $models = [];
        for($i = 0; $i < $this->count; $i++) {
            $initial['id'] = $this->prefix . ($number + $i);
            $model = new UserFake($initial);
            $model->fake($factory);
            $model->generateAuthKey();
            if (!$model->save()) {
                throw new \Exception(json_encode($model->getErrors()));
            }
            $models[] = $model;
        }
        return $models;
    }
}
