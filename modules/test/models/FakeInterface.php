<?php

namespace app\modules\test\models;


use Faker\Generator;

interface FakeInterface
{
    public static function generateData(Generator $factory);
}
