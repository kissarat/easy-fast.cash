<?php

namespace app\modules\test\models;


use app\helpers\SQL;
use app\models\User;
use Faker\Generator;

class UserFake extends User implements FakeInterface
{
    use FakeTrait;

    public static function generateData(Generator $factory) {
        $login = $factory->userName;
        return [
            'referral_id' => static::randomId(),
            'id' => $factory->userName,
            'surname' => $factory->firstName,
            'forename' => $factory->lastName,
            'email' => $login . '@yopmail.com',
            'password' => 1,
            'repeat' => 1,
            'account' => 300,
            'perfect_usd' => 'U12345678'
        ];
    }

    public static function randomId() {
        return SQL::queryCell('SELECT id FROM "user" ORDER BY RANDOM() LIMIT 1');
    }
}
