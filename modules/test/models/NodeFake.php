<?php

namespace app\modules\test\models;

use app\modules\pyramid\models\Node;
use Faker\Generator;

class NodeFake extends Node implements FakeInterface
{
    use FakeTrait;

    public static function generateData(Generator $factory) {
        return [
            'user_id' => UserFake::randomId(),
            'type_id' => rand(1, 4),
        ];
    }
}
