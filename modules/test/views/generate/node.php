<?php
use app\modules\pyramid\models\Type;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\test\models\NodeGeneratorForm $gen */
/** @var \app\modules\pyramid\models\Node[] $models */
?>
<div class="generate-user">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($gen, 'type_id')->dropDownList(Type::names()) ?>
    <?= $form->field($gen, 'count') ?>
    <?= Html::submitButton(Yii::t('app', 'Generate')) ?>

    <?php ActiveForm::end() ?>

    <?php
    $items = [];
    foreach($models as $model) {
        $items[] = implode(' ', [
            $model->id,
            Html::a($model->user_id, ['/user/view', 'id' => $model->user_id]),
        ]);
    }
    echo Html::ul($items, ['encode' => false]);
    ?>
</div>
