<?php
use yii\helpers\Html;

$items = [
    'Users' => ['user'],
    'Investments' => ['node'],
];
?>
<div class="generate-index list">
<?php
foreach($items as $label => $url) {
    echo Html::a($label, $url);
}

echo Html::a(Yii::t('app', 'Clear'), ['clear']);
?>
</div>
