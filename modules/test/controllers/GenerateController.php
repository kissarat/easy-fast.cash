<?php

namespace app\modules\test\controllers;


use app\helpers\SQL;
use app\modules\test\models\NodeGeneratorForm;
use app\modules\test\models\UserGeneratorForm;
use Yii;
use yii\base\Model;
use yii\web\Controller;

class GenerateController extends Controller
{
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionUser() {
        $gen = new UserGeneratorForm();
        return $this->act($gen, 'user');
    }

    public function actionNode() {
        $gen = new NodeGeneratorForm();
        return $this->act($gen, 'node');
    }

    public function actionClear() {
        $count = SQL::execute('DELETE FROM "user" WHERE id <> \'admin\'');
        $journal_count = SQL::execute('DELETE FROM journal');
        SQL::execute('DELETE FROM node');
        $tables = ['node', 'journal'];
        foreach($tables as $table) {
            SQL::execute('ALTER SEQUENCE ' . $table . '_id_seq RESTART');
        }
        if ($count || $journal_count) {
            Yii::$app->session->addFlash('success', Yii::t('app', '{object_number} objects and {journal_number} journal records deleted', [
                'object_number' => $count,
                'journal_number' => $journal_count
            ]));
        }
        return $this->redirect(['index']);
    }

    protected function act(Model $gen, $view) {
        $models = [];
        if ($gen->load(Yii::$app->request->post()) && $gen->validate()) {
            $models = $gen->generate();
        }
        return $this->render($view, [
            'gen' => $gen,
            'models' => $models
        ]);
    }
}
