<?php

namespace app\modules\admin\controllers;


use app\models\User;
use Yii;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionCreate() {
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['/user/index']);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionLogin($auth) {
        /** @var User $model */
        $model = User::findOne(['auth' => $auth]);
        if ($model) {
            Yii::$app->user->login($model);
            return $this->redirect(['/user/view', 'id' => $model->id]);
        }
        else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid code'));
        }
        return $this->goHome();
    }
}
