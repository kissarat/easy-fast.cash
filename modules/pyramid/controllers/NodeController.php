<?php

namespace app\modules\pyramid\controllers;

use app\helpers\SQL;
use app\models\Config;
use app\models\User;
use app\modules\pyramid\models\Node;
use app\modules\pyramid\models\NodeRemain;
use app\modules\pyramid\models\Type;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class NodeController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'pillow', 'unblock'],
                'rules' => [
                    [
                        'actions' => ['pillow', 'unblock'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($type_id = null, $user_id = null) {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $query = NodeRemain::find()
            ->andWhere('number is not null')
            ->andFilterWhere(['type_id' => $type_id])
            ->andFilterWhere(['user_id' => $user_id]);
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC
                    ]
                ]
            ])
        ]);
    }

    public function actionCreate($type_id = null) {
        /** @var Type $type */

        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $model = new Node([
            'user_id' => Yii::$app->user->id,
            'type_id' => $type_id
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $type = Type::findOne($model->type_id);
            if (!$type) {
                throw new NotFoundHttpException();
            }
            $transaction = Yii::$app->db->beginTransaction();
            if ($model->user->account >= $type->price) {
                $model->user->account -= $type->price;
                if ($model->user->save(true, ['account']) && $model->open()) {
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Tariff {type_id} opened', [
                        'type_id' => $model->type_id
                    ]));
                    return $this->redirect(['index']);
                }
            }
        }
        $types = SQL::queryAll('SELECT id, price FROM "type" WHERE :amount >= price', [
            ':amount' => Yii::$app->user->identity->account
        ], PDO::FETCH_KEY_PAIR);
        return $this->render('create', [
            'model' => $model,
            'types' => $types
        ]);
    }

    public function actionPillow() {
        Yii::$app->cache->delete('settings');

        if (isset($_POST['pillow'])) {
            Config::set('common', 'pillow', (float) $_POST['pillow']);
//            SQL::execute('PERFORM (:uid, :ip)', [
//                'uid' => Yii::$app->user->id,
//                'ip' => $_SERVER['REMOTE_ADDR'],
//            ]);
        }
            return $this->render('pillow', [
                'pillow' => Config::get('common', 'pillow')
            ]);

    }

    public function actionSetting(){
        $config = Config::all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $config;
    }

    public function actionPillowCreate() {
        $model = new Node();
        $model->pillow();
            $pillow = Config::get('common', 'pillow');
            $pillow -= 100;
            Config::set('common', 'pillow', $pillow);

        return $this->redirect('pillow');
    }

    public function actionUnblock($user_id) {
        SQL::execute('UPDATE "user" SET number = null WHERE user_id = :uid', [
            ':uid' => $user_id
        ]);
        return $this->goHome();
    }
}
