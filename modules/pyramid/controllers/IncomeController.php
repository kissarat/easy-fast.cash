<?php

namespace app\modules\pyramid\controllers;


use app\modules\pyramid\models\Accrue;
use app\modules\pyramid\models\Node;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class IncomeController extends Controller
{
    public function actionIndex($id) {
        /** @var Node $node */
        $node = Node::findOne($id);
        if (!Yii::$app->user->can('manage') && $node->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException();
        }
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Accrue::find()->where('object_id::INT = :id', [':id' => $id]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ]),
            'node' => $node
        ]);
    }
}
