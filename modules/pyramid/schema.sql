CREATE TABLE type (
  id SMALLINT PRIMARY KEY,
  price DECIMAL(8,2) NOT NULL,
  income DECIMAL(8,2)
);

INSERT INTO "type" VALUES
  (1, 25, 20),
  (2, 80, 120),
  (3, 200, 300),
  (4, 500, 1500);

CREATE TABLE node (
  user_id VARCHAR(24) NOT NULL,
  type_id SMALLINT  NOT NULL,
  count SMALLINT NOT NULL,
  id SERIAL PRIMARY KEY NOT NULL,
  number SERIAL NOT NULL,
  CONSTRAINT node_user FOREIGN KEY (user_id)
  REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT type_number UNIQUE (type_id, number)
);

CREATE TABLE accrue (
  type_id SMALLINT NOT NULL,
  CONSTRAINT accrue_type FOREIGN KEY (type_id)
  REFERENCES "type"(id) ON DELETE CASCADE ON UPDATE NO ACTION
) INHERITS (transfer);


CREATE OR REPLACE VIEW node_remain AS
  SELECT node.*, sum(count) over (partition by type_id ORDER BY number) as remain FROM node;


CREATE OR REPLACE FUNCTION enter(
  uid VARCHAR(24),
  tid SMALLINT,
  ip INET DEFAULT NULL,
  _id INT DEFAULT NULL) RETURNS INT AS $$
DECLARE
  _type RECORD;
BEGIN
  SELECT * FROM "type" WHERE id = tid INTO _type;
  UPDATE node SET count = count - 1 WHERE id IN (SELECT min(id) FROM node WHERE type_id = tid);
  IF _id IS NULL THEN
    INSERT INTO node VALUES (uid, tid, 4) RETURNING id INTO _id;
    PERFORM go(uid, ip);
  ELSE
    UPDATE node SET type_id = tid, number = DEFAULT, count = 4 WHERE id = _id;
  END IF;
  RETURN _id;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION go(uid VARCHAR(24), ip INET) RETURNS VOID AS $$
DECLARE
  _id INT;
BEGIN
  LOOP
    CREATE TEMPORARY TABLE up AS
      SELECT user_id, type_id, n.id as object_id, income as amount
      FROM node n JOIN "type" t ON type_id = t.id WHERE count <= 0 AND number is not null
      ORDER BY n.id;
    IF (SELECT count(*) <= 0 FROM up) THEN
      DROP TABLE up;
      RETURN;
    END IF;
    UPDATE "user" u SET account = account + amount FROM up WHERE u.id = up.user_id;
    UPDATE up SET type_id = type_id + 1 WHERE type_id < 4;
    INSERT INTO accrue("type", "event", user_id, object_id, ip, amount, type_id)
      SELECT 'node', 'accrue', user_id, object_id::VARCHAR, ip, amount, type_id FROM up;
    SELECT enter(uid, type_id, ip, object_id) FROM up;
    DROP TABLE up;
  END LOOP;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE FUNCTION pillow(uid VARCHAR(24), ip INET) RETURNS VOID AS $$
DECLARE
  _type RECORD;
  _amount DECIMAL(8,2);
BEGIN
  SELECT * FROM "type" WHERE id = 1 INTO _type;
  LOOP
    SELECT "value"::DECIMAL(8,2) FROM "setting" WHERE "category" = 'common' AND "name" = 'pillow' INTO _amount;
    IF _amount < _type.price THEN
      RETURN;
    END IF;
    _amount := _amount - _type.price;
    UPDATE "setting" SET value = _amount WHERE "category" = 'common' AND "name" = 'pillow';
    UPDATE node SET count = count - 1 WHERE id = (SELECT min(id) FROM node WHERE type_id = 1);
    PERFORM go(uid, ip);
  END LOOP;
END
$$ LANGUAGE plpgsql VOLATILE;


CREATE FUNCTION cron() RETURNS VOID AS $$
DECLARE
  _user RECORD;
BEGIN
  FOR _user IN SELECT * FROM "user" WHERE now() - "time" > interval '1 week' LOOP
    IF _user.account >= 2 THEN
      UPDATE "user" SET account = account - 2 WHERE id = _user.id;
    ELSE
      UPDATE node SET number = NULL WHERE user_id = _user.id;
    END IF;
  END LOOP;
END
$$ LANGUAGE plpgsql VOLATILE;

INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'pillow', 0);
