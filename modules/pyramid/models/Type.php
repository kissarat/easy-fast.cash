<?php

namespace app\modules\pyramid\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property number $price
 * @property number $income
 */
class Type extends ActiveRecord
{
    public function __toString()
    {
        return Yii::t('app', 'Tariff') . ' ' . $this->id;
    }

    public static function names() {
        $names = [];
        foreach(Type::find()->orderBy('id')->each() as $type) {
            $names[$type->id] = $type->__toString();
        }
        return $names;
    }
}

