<?php

namespace app\modules\pyramid\models;


use app\models\Transfer;
use app\models\UserRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $number
 * @property integer $count
 * @property integer $type_id
 * @property string user_id
 *
 * @property Type type
 */
class Node extends UserRecord
{
    public static function tableName()
    {
        return 'node';
    }

    public function rules()
    {
        return [
            [['type_id', 'user_id'], 'required'],
            [['type_id', 'number', 'count'], 'integer'],
            ['user_id', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type_id' => Yii::t('app', 'Tariff'),
            'user_id' => Yii::t('app', 'User'),
            'count' => Yii::t('app', 'Counter'),
            'remain' => Yii::t('app', 'Remain'),
            'id' => 'ID',
        ];
    }

    public function getType() {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function pillow(){
        $number = Node::find()
            ->where(['type_id' => 1])
            ->min('number');
        if ($number) {
            /** @var Node $expectant */
            $expectant = Node::find()
                ->where([
                    'type_id' => 1,
                    'number' => $number,
                ])
                ->one();
            /** @var Node $expectant */
                        $expectant->type_id = 2;
                    $expectant->save();
        }
    }

    public function open() {
        $number = Node::find()
            ->where(['type_id' => $this->type_id])
            ->min('number');
        if ($number) {
            /** @var Node $expectant */
            $expectant = Node::find()
                ->where([
                    'type_id' => $this->type_id,
                    'number' => $number,
                ])
                ->one();
            /** @var Node $expectant */
            if ($expectant) {
                $expectant->count--;
                if ($expectant->count <= 0 && $expectant->accrue(Yii::$app->user->id)) {
                    if ($expectant->type_id < 4) {
                        $expectant->type_id = $expectant->type_id + 1;
                    }
                    if ($this != $expectant) {
                        $expectant->open();
                    }
                }
                else {
                    $expectant->save();
                }
            }
        }

        if (empty($this->count) || $this->count <= 0) {
            $this->number = Node::find()
                ->where(['type_id' => $this->type_id])
                ->max('number');
            if (empty($this->number)) {
                $this->number = 0;
            }
            $this->number++;
            $this->count = 4;
            if ($this->save()) {
                $referral = $this->user->referral;
                if ($referral) {
                    $referral->account += $this->type->price * 0.1;
                    return $referral->save(false);
                }
                else {
                    return true;
                }
            }
        }
        return false;
    }

    public function accrue($user_id) {
        $amount = $this->type->income;
        $transfer = new Accrue([
            'type' => 'node',
            'event' => 'accrue',
            'user_id' => $user_id,
            'object_id' => $this->id,
            'amount' => $amount,
            'type_id' => $this->type_id,
            'ip' => Yii::$app->request->userIP
        ]);
        if ($transfer->save()) {
            $this->user->account += $amount;
            return $this->user->save(false);
        }
        return false;
    }
}
