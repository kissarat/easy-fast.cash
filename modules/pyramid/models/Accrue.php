<?php

namespace app\modules\pyramid\models;


use app\models\Transfer;
use Yii;

/**
 * @property string $type_id
 */
class Accrue extends Transfer
{
    public static function tableName() {
        return 'accrue';
    }

    public function getType() {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function attributeLabels()
    {
        return [
            'time' => Yii::t('app', 'Time'),
            'amount' => Yii::t('app', 'Amount'),
            'type_id' => Yii::t('app', 'Tariff'),
        ];
    }
}
