<?php
use yii\helpers\Html;
?>
<div class="pyramid-pillow">
    <form action="pillow" method="post">
        <?= Html::textInput('pillow', $pillow) ?>
        <?= Html::submitButton(Yii::t('app', 'Change')) ?>
    </form>
    <h3><?=Yii::t('app','Current pillow value')?></h3>
    <p><?=$pillow?></p>
    <?=Html::a(Yii::t('app','Raise user'),['pillow-create'],['class'=>'btn btn-primary'])?>
</div>
