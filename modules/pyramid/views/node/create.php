<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$list = [];
foreach($types as $id => $price) {
    $list[$id] = Yii::t('app', 'Tariff') . " (\$$price)";
}
?>
<div class="node-create">
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'type_id')->dropDownList($list);
    echo Html::submitButton(Yii::t('app', 'Open'));
    ActiveForm::end();
    ?>
</div>
