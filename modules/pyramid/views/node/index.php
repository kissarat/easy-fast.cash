<?php
use app\modules\pyramid\models\Node;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app','Investments');

$columns = ['id'];

if (empty($_GET['type_id'])) {
    $columns[] = [
        'attribute' => 'type_id',
        'value' => function(Node $model) {
            return Yii::t('app', 'Tariff') . ' ' . $model->type_id;
        }
    ];
}

if (empty($_GET['user_id'])) {
    $columns[] = [
        'attribute' => 'user_id',
        'format' => 'html',
        'value' => function(Node $model) {
            return Html::a($model->user_id, ['index', 'user_id' => $model->user_id]);
        }
    ];
}

$columns[] = 'remain';

$columns[] = [
    'label' => Yii::t('app', 'Action'),
    'format' => 'html',
    'value' => function(Node $model) {
        return Html::a('', ['/pyramid/income/index', 'id' => $model->id], [
            'title' => Yii::t('app', 'Accrue'),
            'class' => 'fa fa-usd'
        ]);
    }
];

?>
<div class="node-index">
    <?= GridView::widget([
    'dataProvider' => $dataProvider,
        'rowOptions'=>function($data){
            if($data->number == null){
                return ['class' => 'danger'];
            }
        },
    'columns' => $columns
]) ?>
</div>
