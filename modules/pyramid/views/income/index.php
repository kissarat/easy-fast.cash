<?php
use app\modules\pyramid\models\Accrue;
use yii\grid\GridView;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\pyramid\models\Node $node */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Investments'), 'url' =>
    ['/pyramid/node/index', 'user_id' => $node->user_id]];
?>
<div class="income-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'time:datetime',
            'amount:integer',
            [
                'attribute' => 'type_id',
                'value' => function(Accrue $model) {
                    return Yii::t('app', 'Tariff') . ' ' . $model->type_id;
                }
            ]
        ]
    ]);
    ?>
</div>
