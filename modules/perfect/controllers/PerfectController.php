<?php

namespace app\modules\perfect\controllers;


use app\controllers\InvoiceController;
use app\models\Invoice;
use app\models\Config;
use Yii;
use yii\web\Response;

class PerfectController extends InvoiceController
{
    public function actionPay($id) {
        return $this->render('pay', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionHash() {
        $data = file_get_contents('php://input');
        $data = str_replace("\n", ":", $data);
        Yii::$app->response->format = Response::FORMAT_RAW;
        return strtoupper(md5($data));
    }

    protected function success(Invoice $model) {
        if (!$model->withdraw) {
            if ($this->validate()) {
                $model->wallet = $_POST['PAYER_ACCOUNT'];
                $model->batch = $_POST['PAYMENT_BATCH_NUM'];
                $model->amount = (float) $_POST['PAYMENT_AMOUNT'];
                return true;
            }
        }
        return false;
    }

    protected function validate() {
        $fields = [
            'PAYMENT_ID' => '/^\d+$/',
            'PAYEE_ACCOUNT' => '/^U\d{7,8}$/',
            'PAYMENT_AMOUNT' => '/^\d+(\.\d{2})?$/',
            'PAYMENT_UNITS' => '/^USD$/',
            'PAYMENT_BATCH_NUM' => '/^\d+$/',
            'PAYER_ACCOUNT' => '/^U\d{7,8}$/',
            'TIMESTAMPGMT' => '/^\d+$/',
            'V2_HASH' => '/^[0-9A-Z]+$/',
        ];
        foreach($fields as $name => $pattern) {
            if (empty($_POST[$name]) || !preg_match($pattern, $_POST[$name])) {
                return false;
            }
        }

        if (Config::get('perfect', 'wallet') != $_POST['PAYEE_ACCOUNT']) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid payee'));
            return false;
        }

        $string = implode(':', [
            $_POST['PAYMENT_ID'],
            $_POST['PAYEE_ACCOUNT'],
            $_POST['PAYMENT_AMOUNT'],
            $_POST['PAYMENT_UNITS'],
            $_POST['PAYMENT_BATCH_NUM'],
            $_POST['PAYER_ACCOUNT'],
            strtoupper(md5(Config::get('perfect', 'secret'))),
            $_POST['TIMESTAMPGMT']
        ]);

        if ($_POST['V2_HASH'] != strtoupper(md5($string))) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid hash'));
            return false;
        }

        return true;
    }
}
