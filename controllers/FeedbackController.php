<?php

namespace app\controllers;

use app\helpers\God;
use app\helpers\SQL;
use app\models\Feedback;
use app\models\Message;
use Yii;
use app\models\search\Feedback as FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class FeedbackController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'close' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $vars = [];
        if (!Yii::$app->user->getIsGuest()) {
            $searchModel = new FeedbackSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $vars = [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ];
        }

        return $this->render('index', $vars);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionTicket($ticket) {
        /* @var Feedback $topic */
        $topic = Feedback::findOne(['ticket' => $ticket]);
        if (!$topic) {
            throw new NotFoundHttpException();
        }
        $new = null;
        if ($topic->open) {
            $new = new Message();
            if (!Yii::$app->user->getIsGuest()) {
                $new->user_id = Yii::$app->user->id;
                if (Yii::$app->user->can('manage')) {
                    $new->setScenario('answer');
                    $new->answer = true;
                }
            }
            if ($new->load(Yii::$app->request->post())) {
                $new->feedback_id = $topic->id;
                if ($new->save()) {
                    $new->images = UploadedFile::getInstances($new, 'images');
                    $new->upload();
                    if ($new->answer && $topic->email) {
                        God::mail($topic->email, 'feedback', [
                            'subject' => 'Ответ администрации',
                            'ticket' => $topic->ticket,
                            'message' => $new->content
                        ]);
                    }
                    $new = new Message();
                }
            }
        }

        $messages = $topic->getMessages()
            ->orderBy(['time' => SORT_ASC])
            ->all();

        return $this->render('ticket', [
            'topic' => $topic,
            'messages' => $messages,
            'new' => $new
        ]);
    }

    public function actionClose($ticket) {
        SQL::execute('UPDATE "feedback" SET "open" = false WHERE ticket = :ticket', [':ticket' => $ticket]);
        return $this->redirect(['ticket', 'ticket' => $ticket]);
    }

    public function actionCreate() {
        $model = new Feedback(['scenario' => Yii::$app->user->getIsGuest() ? 'guest' : 'default']);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            $model->ticket = God::generateRandomLiteral(24);
            $message = new Message([
                'scenario' => 'default',
                'content' => $model->content
            ]);
            if ('default' == $model->scenario) {
                $message->user_id = Yii::$app->user->id;
            }
            if ($model->save()) {
                $message->feedback_id = $model->id;
                $message->ip = $_SERVER['REMOTE_ADDR'];
                if ($message->save()) {
                    $transaction->commit();
                    if ('guest' == $model->getScenario()) {
                        setcookie('ticket', $model->ticket, time() + 86400 * 365, '/');
                    }
                    return $this->redirect(['ticket', 'ticket' => $model->ticket]);
                }
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        Message::deleteAll(['feedback_id' => $id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
