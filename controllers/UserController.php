<?php

namespace app\controllers;


use app\helpers\SQL;
use app\models\LoginForm;
use app\models\PasswordForm;
use app\models\RequestForm;
use app\models\User;
use app\models\WalletForm;
use app\modules\pyramid\models\Node;
use kartik\base\Config;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller {
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'login', 'logout', 'request', 'reset', 'password', 'reset-auth'],
                'rules' => [
                    [
                        'actions' => ['create', 'login', 'request', 'reset'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['logout', 'update', 'password'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['index', 'reset-auth'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ]
            ]
        ];
    }

    public function actionIndex($referral_id = null) {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => User::find()->andFilterWhere(['referral_id' => $referral_id]),
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ])
        ]);
    }
    public function actionUnblock($user_id){
        $pillow = \app\models\Config::get('common','pillow');
        \app\models\Config::set('common','pillow',(float)$pillow +2);
        $type = Node::find()->where(['user_id'=>$user_id])->select('type_id')->scalar();
        $node = Node::find()->andWhere(['type_id'=>$type])->select('max(number)')->scalar();
        SQL::execute('UPDATE "node" SET "number" = :nmb WHERE "user_id" = :uid',[
            ':nmb'=>$node+1,
            ':uid'=>$user_id
        ]);
        SQL::execute('UPDATE "user" SET "time" = now() WHERE "id" = :uid',[
            ':uid'=>$user_id
        ]);
        return $this->redirect('index');
    }

    public function actionLogin() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user && $user->validatePassword($model->password)) {
                $node = new Node();
                $node = $node->find()->where(['user_id'=>$user->id])->one();
                if($node) {
                    if ($node->number == null) {
                        return $this->render('block');
                    }
                }
                if (empty($user->auth)) {
                    $user->generateAuthKey();
                    $user->save(true, ['auth']);
                }
                if (Yii::$app->user->login($user)) {
                    return $this->redirect(['view', 'id' => $user->id]);
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout($destroy = false) {
        Yii::$app->user->logout($destroy);
        return $this->goHome();
    }

    public function actionResetAuth($id) {
        if (!Yii::$app->user->can('manage')) {
            if ($id != Yii::$app->user->id) {
                throw new ForbiddenHttpException();
            }
            Yii::$app->layout = 'cabinet';
        }
        $model = $this->findModel($id);
        $model->generateAuthKey();
        if ($model->save(false)) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Logout done'));
        }
        return $this->goHome();
    }

    public function actionCreate($referral_id = null) {
        $model = new User(['referral_id' => $referral_id]);
        Yii::$app->view->title = Yii::t('app', 'Signup');
        return $this->edit($model);
    }

    public function actionUpdate($id = null) {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $model = $this->findModel($id);
        Yii::$app->view->title = Yii::t('app', 'Update');
        return $this->edit($model);
    }

    public function actionView($id = null) {
        if (empty($id)) {
            return $this->redirect(['user/view', 'id' => Yii::$app->user->id]);
        }
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->layout = 'cabinet';
        }
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function findModel($id = null) {
        /** @var User $model */
        if ($id) {
            $model = User::findOne($id);
            if (!$model) {
                throw new NotFoundHttpException();
            }
        }
        else {
            $model = Yii::$app->user->identity;
        }
        return $model;
    }

    public function actionPassword($id) {
        if (!Yii::$app->user->can('manage')) {
            if ($id != Yii::$app->user->id) {
                throw new ForbiddenHttpException();
            }
            Yii::$app->layout = 'cabinet';
        }
        $user = $this->findModel($id);
        $model = new PasswordForm();
        if ($this->password($user, $model)) {
            return $this->redirect(['view', 'id' => $id]);
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }

    public function actionReset($code) {
        /** @var User $user */
        $user = User::findOne(['code' => $code]);
        if (!$user) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Invalid code'));
            return $this->goHome();
        }
        $model = new PasswordForm();
        $user->code = null;
        if ($this->password($user, $model)) {
            return $this->redirect(['login']);
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }

    public function actionRequest() {
        $model = new RequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user) {
                $user->generateCode();
                if ($user->save(false)) {
                    $url = Url::to(['reset', 'code' => $user->code], true);
                    $content = Yii::t('app', 'To recover your password, open <a href="{url}">this link</a>', [
                        'url' => $url,
                    ]);
                    $user->sendEmail([
                        'subject' => Yii::$app->name . ': ' . Yii::t('app', 'Password reset'),
                        'content' => $content
                    ]);
                    Yii::$app->session->addFlash('info', Yii::t('app', 'Check your mail'));
                    return $this->goHome();
                }
            }

            if (empty($model->id) && empty($model->email)) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Enter login or email'));
            }
        }

        return $this->render('request', [
            'model' => $model
        ]);
    }

    public function actionWallet($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save(true, $model->walletAttributes())) {
            return $this->redirect(['view', 'id' => $id]);
        }
        return $this->render('wallet', [
            'model' => $model
        ]);
    }

    protected function edit(User $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save(false) && Yii::$app->user->login($model)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    protected function password(User $user, PasswordForm $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->setPassword($model->password);
            return $user->save(true, ['hash', 'auth', 'code']);
        }
        return false;
    }
}
