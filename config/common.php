<?php

define('ROOT', __DIR__ . '/..');

$config = [
    'id' => 'easy_fast_cash',
    'name' => 'Easy-Fast.Cash',
    #'timeZone' => 'Europe/Moscow',
    'basePath' => __DIR__ . '/..',
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'charset' => 'utf-8',
    'layout' => 'main',
    'params' => null,
];

$dbname = 'easy_fast_cash';
$config['components'] = [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'pgsql:host=localhost;dbname=' . $dbname,
        'username' => $dbname,
        'password' => $dbname,
        'charset' => 'utf8',
        'enableSchemaCache' => true
    ],

    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.yandex.ru',
            'username' => 'kissarat@yandex.ru',
            'password' => 'kuro4ka',
            'port' => 465,
            'encryption' => 'ssl',

        ],
    ],

    'authManager' => [
        'class' => 'yii\rbac\DbManager',
    ],

    'local' => [
        'class' => 'creocoder\flysystem\LocalFilesystem',
        'path' => '@app',
    ]
];
