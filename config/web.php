<?php

require_once 'common.php';

if (empty($_COOKIE['lang'])) {
    $language = empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? '' : $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    $language = preg_match('/(ru|uk|be|ky|ab|mo|et|lv)/i', $language) ? 'ru' : 'en';
}
else {
    $language = $_COOKIE['lang'];
}

$config['language'] = $language;

$config['components'] = array_merge($config['components'], [
    'cache' => [
        'class' => 'yii\caching\ApcCache',
        'keyPrefix' => 'd'
    ],

    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\DbTarget',
                'levels' => ['error', 'warning'],
                'enabled' => false
            ],
        ],
    ],

    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'enableStrictParsing' => false,
        'rules' => [
        ],
    ],

    'i18n' => [
        'translations' => [
            'app*' => [
                'class' => 'yii\i18n\DbMessageSource',
//                'enableCaching' => true
            ]
        ]
    ],

    'session' => [
        'class' => 'yii\web\CacheSession',
        'name' => 'auth'
    ],
/*
    'backup' => 'app\components\Backup',
    'perfect' => 'app\modules\invoice\components\PerfectMoney',
    'nix' => 'app\modules\invoice\components\NixMoney',
    'advcash' => 'app\modules\invoice\components\Advcash',
*/
    'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'dateFormat' => 'php:d-m-Y',
        'datetimeFormat' => 'php:d-m-Y H:i',
        'timeFormat' => 'php:H:i:s',
    ],

    'request' => [
        'cookieValidationKey' => 'baef8ek1QuiQu6Ol9zaiShaiche8zu3B',
        'enableCsrfValidation' => true
    ],

    'errorHandler' => [
        'errorAction' => 'home/error',
    ],

    'user' => [
        'identityClass' => 'app\models\User',
        'enableAutoLogin' => true,
        'loginUrl' => ['user/login'],
        'on afterLogin' => function($event) {
            /* @var $user \app\models\User */
            /*$user = Yii::$app->user->identity;
            $event->name = 'login';
            if ($user->isManager()) {
                $ips = file_get_contents(Yii::getAlias('@app/config/hosts.allow'));
                $ips = explode("\n", $ips);
                if (!in_array($_SERVER['REMOTE_ADDR'], $ips)) {
                    $event->name = 'blocked';
                }
            }
            $event->sender = $user;
            Journal::writeEvent($event);
            if ($user->isManager()) {
                setcookie('role', 'manage');
            }
            if ('blocked' == $event->name) {
                Yii::$app->session->addFlash('error', 'Доступ запрещен');
                header('Location: /login');
                exit();
            }
*/
        },
        'on beforeLogout' => function($event) {
            /*
            $event->name = 'logout';
            $event->sender = Yii::$app->user->identity;
            Journal::writeEvent($event);
            */
        },
    ],
]);

$config['modules']['perfect'] = 'app\modules\perfect\Module';
$config['bootstrap'][] = 'perfect';

$config['modules']['lang'] = 'app\modules\lang\Module';
$config['bootstrap'][] = 'lang';

$config['modules']['pyramid'] = 'app\modules\pyramid\Module';
$config['bootstrap'][] = 'pyramid';

$config['modules']['test'] = 'app\modules\test\Module';
$config['bootstrap'][] = 'test';

$config['modules']['review'] = 'app\modules\review\Module';
$config['bootstrap'][] = 'review';

$config['modules']['admin'] = 'app\modules\admin\Module';
$config['bootstrap'][] = 'admin';

if (YII_DEBUG) {
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '194.44.188.18'],
    ];
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['bootstrap'][] = 'gii';
    $config['bootstrap'][] = 'debug';
}
