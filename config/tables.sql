CREATE TYPE confidant AS ENUM('father', 'mother', 'brother',
'sister', 'husband', 'wife', 'son', 'daughter', 'other');

CREATE TABLE "user" (
  id                   VARCHAR(24)   NOT NULL,
  account              DECIMAL(8, 2) NOT NULL DEFAULT '0.00',
  email                VARCHAR(48)   NOT NULL,
  hash                 CHAR(60),
  auth                 CHAR(64),
  code                 CHAR(64),
  referral_id          VARCHAR(24),
  forename             VARCHAR(36),
  surname              VARCHAR(36),
  time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT uq_user_id UNIQUE ("id"),
  CONSTRAINT uq_user_auth UNIQUE ("auth"),
  CONSTRAINT uq_user_code UNIQUE ("code")
);

INSERT INTO "user"(id, email, hash) VALUES
('admin', 'lab_tas@ukr.net', '$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W');


CREATE TABLE journal (
  id        SERIAL PRIMARY KEY NOT NULL,
  type      VARCHAR(16)        NOT NULL,
  event     VARCHAR(16)        NOT NULL,
  user_id   VARCHAR(24),
  object_id VARCHAR(40),
  ip        INET,
  time      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT journal_user FOREIGN KEY (user_id)
  REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE transfer (
  amount DECIMAL(8, 2) NOT NULL DEFAULT '0.00'
) INHERITS (journal);

CREATE TYPE invoice_type AS ENUM ('perfect', 'nix', 'advcash');

CREATE TABLE invoice (
  id      SERIAL PRIMARY KEY NOT NULL,
  user_id VARCHAR(24)      NOT NULL,
  amount  DECIMAL(8, 2)    NOT NULL,
  type    invoice_type     NOT NULL DEFAULT 'perfect',
  status  VARCHAR(16) DEFAULT 'create',
  batch   VARCHAR(40),
  wallet  VARCHAR(40),
  withdraw BOOL,
  CONSTRAINT invoice_user FOREIGN KEY (user_id)
  REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE SEQUENCE text_id_seq;

CREATE TABLE "text" (
  id VARCHAR(24) NOT NULL DEFAULT nextval('text_id_seq')::VARCHAR(24),
  lang TEXT,
  content TEXT,
  PRIMARY KEY (id, lang)
);

CREATE TABLE article (
  number INT NOT NULL DEFAULT 0,
  type VARCHAR(24),
  title VARCHAR(256) NOT NULL,
  keywords VARCHAR(192),
  summary TEXT
) INHERITS (text);

CREATE TABLE "feedback" (
  id      SERIAL PRIMARY KEY NOT NULL,
  ticket  CHAR(24) UNIQUE    NOT NULL,
  email   VARCHAR(48),
  subject VARCHAR(256)       NOT NULL,
  open    BOOL               NOT NULL DEFAULT TRUE
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING BTREE ("id");
CREATE UNIQUE INDEX feedback_ticket ON "feedback" USING BTREE ("ticket");


CREATE TABLE "feedback_message" (
  id          SERIAL PRIMARY KEY NOT NULL,
  feedback_id INT                NOT NULL,
  user_id   VARCHAR(24),
  answer      BOOL               NOT NULL DEFAULT FALSE,
  "content"   TEXT               NOT NULL,
  "time"      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip          INET,
  CONSTRAINT feedback_user FOREIGN KEY (user_id)
  REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE NO ACTION
);


CREATE TABLE "setting" (
  "id"       SERIAL PRIMARY KEY,
  "user_id"  VARCHAR(24),
  "visible"  BOOLEAN NOT NULL DEFAULT true,

  "category" VARCHAR(24) NOT NULL,
  "name"     VARCHAR(24) NOT NULL,
  "type"     VARCHAR(24),
  "value"    TEXT,
  CONSTRAINT feedback_user FOREIGN KEY (user_id)
  REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'email', 'kissarat@yandex.ru');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'login_fails', 5);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'skype', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'google', NULL);
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'yandex', '34013255');
INSERT INTO "setting" ("category", "name", "value") VALUES ('common', 'heart', NULL);
INSERT INTO "setting" ("category", "name", "type", "value")
VALUES ('common', 'withdrawal_confirmation', 'bool', TRUE);
