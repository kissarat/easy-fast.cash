<?php

namespace app\helpers;


use Yii;

class Country
{
    private static $_countries;
    public static function &getCountries() {
        if (!static::$_countries) {
            static::$_countries = json_decode(
                file_get_contents(
                    Yii::getAlias('@app/web/data/country/' . Yii::$app->language . '.json')), true);
        }
        return static::$_countries;
    }

    public static function decode($code) {
        $countries = static::getCountries();
        if (empty($countries[$code])) {
            return $code;
        }
        else {
            return $countries[$code];
        }
    }
}
