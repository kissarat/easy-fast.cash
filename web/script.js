$('select[placeholder]').each(function(i, select) {
    $('<option />')
        .text(select.getAttribute('placeholder'))
        .attr('disabled', true)
        .attr('selected', true)
        .prependTo(select);
});

$('[name=User]').on('beforeSubmit', function(e) {
    if (agree.checked) {
        return true;
    }
    alert('You must confirm agreement');
    return false;
});

if (window.moment && window.last) {
    last.innerHTML = moment.unix(last.dataset.left).fromNow();
}
$(document).ready(function(){
    var c = $(".wrap>.container");
    var h =c.position();
    var height = $(window).height() - h.top - $("footer").height();
    //alert($(window).height());
    $(".wrap>.container").css({
        "min-height": height
    });
});

