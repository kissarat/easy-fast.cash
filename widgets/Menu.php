<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class Menu extends Widget
{
    public $items;

    public static function menu(array $items) {
        $anchors = [];
        foreach($items as $label => $url) {
            if (empty($url['url'])) {
                $anchors[] = Html::a($label, $url);
            }
            else {
                $anchors[] = Html::a(
                    isset($url['label']) ? $url['label'] : '',
                    $url['url'],
                    isset($url['options']) ? $url['options'] : []
                );
            }
        }
        return implode("\n", $anchors);
    }

    public function run() {
        echo Html::beginTag('dl', [
            'class' => 'collapsible'
        ]);
        foreach($this->items as $label => $item) {
            if(is_string($label)) {
                echo Html::tag('dt', Html::a($label, $item));
            }
            elseif (is_array($item)) {
                $options = [];
                if(isset($item['match']) && preg_match($item['match'], Yii::$app->request->getPathInfo())) {
                    $options['class'] = 'active';
                }
                echo Html::tag('dt', $item['header'], $options);
                echo Html::tag('dd', is_array($item['content']) ? static::menu($item['content']) : $item['content']);
            }
            else {
                echo Html::tag('dt', $item);
            }
        }
        echo Html::endTag('dl');
    }
}
