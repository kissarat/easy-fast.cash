<?php

namespace app\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class Fake extends Widget
{
    public function run() {
        if (Yii::$app->user->can('manage')) {
            echo Html::button(Yii::t('app', 'Fake'), [
                'onclick' => 'fake()'
            ]);
        }
    }
}
