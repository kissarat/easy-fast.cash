<?php
use Firebase\FirebaseLib;

require_once __DIR__ . '/../vendor/autoload.php';
require_once 'pdo.php';

$params = [
    'action:',
    'table:',
    'columns::',
    'host::',
    'id::',
];

$options = getopt('a:t:c:h:i:z');

$fb = new FirebaseLib('https://back.firebaseio.com/', 'exwkFWw6GWtNCqYoQAK8cS5ATzSvFyENdB4AnioH');
$pdo = connect();

function id($id) {
    return str_replace('.', '_', $id);
}

define('DOMAIN', id(isset($options['h']) ? $options['h'] : $config['id']));

function path($id) {
    global $options;
    return DOMAIN . "/" . $options['t'] . "/" . id($id);
}

switch($options['a']) {
    case 'up':
        $columns = isset($options['c']) ? $options['c'] : '*';
        foreach(explode(',', $options['t']) as $table) {
            $r = $pdo->query('SELECT ' . $columns . ' FROM "' . $table . '" ORDER BY id');
            $rows = [];
            $i = 0;
            while ($record = $r->fetch(PDO::FETCH_ASSOC)) {
                $object = [];
                $id = isset($options['z']) ? ++$i : $record['id'];
                foreach ($record as $key => $value) {
                    if (null !== $value && '' !== $value) {
                        if (is_numeric($value)) {
                            $value = (float)$value;
                        }
                        $object[$key] = $value;
                    }
                }
                $rows[$id] = $object;
            }
            $fb->set(DOMAIN . "/" . $table, $rows);
        }
        break;

    case 'down':
        foreach(explode(',', $options['t']) as $table) {
            $rows = $fb->get(DOMAIN . '/' . $table);
            $rows = json_decode($rows, true);
            foreach ($rows as $row) {
                if (empty($row)) {
                    continue;
                }
                $names = array_keys($row);
                $params = [];
                foreach ($row as $key => $value) {
                    $params[':' . $key] = $value;
                }
                $names = implode(',', $names);
                $placeholders = implode(',', array_keys($params));
                $r = $pdo->prepare("INSERT INTO $table ($names) VALUES ($placeholders)");
                foreach ($row as $key => $value) {
                    $params[':' . $key] = $value;
                }
                $r->execute($params);
            }
        }
        break;

    case 'get':
        $object = $fb->get(id($options['i']));
        $object = json_decode($object, true);
        echo json_encode($object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n";
        break;

    case 'delete':
        $fb->delete(id($options['i']));
}