<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property string id
 * @property string lang
 * @property string content
 */
class Text extends ActiveRecord
{
    public static function tableName()
    {
        return 'text';
    }

    public function init() {
        $this->on(static::EVENT_AFTER_INSERT, [$this, 'saveFile']);
        $this->on(static::EVENT_AFTER_UPDATE, [$this, 'saveFile']);
    }

    public static function primaryKey() {
        return ['id', 'lang'];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'keywords' => Yii::t('app', 'Keywords'),
            'summary' => Yii::t('app', 'Summary'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    public static function languages() {
        return [
            'ru' => 'Русский',
            'en' => 'English'
        ];
    }

    public function getSafeId() {
        return str_replace('/', '-', $this->id);
    }

    public function saveFile() {
        $attributes = [];
        foreach($this->getAttributes() as $key => $value) {
            if (!empty($value)) {
                $attributes[$key] = $value;
            }
        }
        $attributes['_meta'] = [
            'type' => static::tableName(),
            'time' => time(),
//            'user_id' => Yii::$app->user->id,
            'host' => gethostname()
        ];
        file_put_contents($this->getFilename(),
            json_encode($attributes, JSON_UNESCAPED_UNICODE));
    }

    public function loadFile() {
        $filename = $this->getFilename();
        if (is_file($filename)) {
            $content = file_get_contents($filename);
            $attributes = json_decode($content, true);
            if ($this->id == $attributes['id'] && static::tableName() == $attributes['_meta']['type']) {
                unset($attributes['_meta']);
                $this->setAttributes($attributes);
                return $this->save();
            }
        }
        return false;
    }

    public function getFilename() {
        $file = $this->getSafeId();
        $lang = $this->lang or Yii::$app->language;
        return Yii::getAlias("@app/web/data/text/$lang/$file.json");
    }
}
