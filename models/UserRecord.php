<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property string user_id
 * @property User $user
 */
abstract class UserRecord extends ActiveRecord
{
    public function getUser() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }


    public function __debuginfo() {
        $info = [
            'model' => $this->attributes,
            'errors' => $this->getErrors()
        ];
        return json_encode($info, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function dumpErrors() {
        foreach($this->getErrors() as $name => $message) {
            Yii::$app->session->addFlash('error', $name . ': ' . $message[0]);
        }
    }
}
