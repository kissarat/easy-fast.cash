<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\Feedback as FeedbackModel;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Feedback represents the model behind the search form about `app\models\Feedback`.
 */
class Feedback extends FeedbackModel
{
    public function rules() {
        return [
            [['id'], 'integer'],
            [['username', 'email', 'subject', 'content'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * 
     * @return ActiveDataProvider
     */
    public function search($params) {
        $manager = Yii::$app->user->can('manage');
        $query = $manager ? FeedbackModel::find()
            : FeedbackModel::findBySql('SELECT f.id, email, subject, (
            SELECT user_id FROM feedback_message WHERE feedback_id = f.id ORDER BY user_id DESC LIMIT 1
          ), ticket, "time"
          FROM feedback f JOIN (
              SELECT feedback_id, max("time") as "time"
              FROM feedback_message GROUP BY feedback_id ORDER BY "time" DESC
          ) m ON f.id = feedback_id
          ORDER BY "time"');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'subject', $this->subject]);

        return $dataProvider;
    }
}
