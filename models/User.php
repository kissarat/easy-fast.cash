<?php

namespace app\models;


use app\helpers\God;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property string id
 * @property string email
 * @property string hash
 * @property string auth
 * @property string code
 * @property number account
 * @property string password
 * @property string referral_id
 * @property string surname
 * @property string forename
 *
 * @property User referral
 */
class User extends ActiveRecord implements IdentityInterface {

    private $_password;
    public $repeat;

    const LOGIN = '/^[a-z][\w\.]{3,22}\w$/';
    const PERFECT = '/^U\d{7,8}$/';

    public static function tableName()
    {
        return 'user';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules() {
        return [
            [['id', 'email', 'referral_id', 'forename', 'surname', 'perfect_usd'], 'required'],
//            [['password', 'repeat'], 'required', 'on' => 'signup'],
            ['id', 'string'],
            ['email', 'email'],
            [['hash', 'auth', 'code'], 'string'],
            [['id', 'referral_id'], 'match', 'pattern' => static::LOGIN],
            ['account', 'default', 'value' => 0],
            ['id', 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['referral_id', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('app', 'The referral does not exists')],
            ['perfect_usd', 'match', 'pattern' => static::PERFECT],
        ];
    }
/*
    public function scenarios()
    {
        return [
            'default' => ['id', 'email', 'password', 'referral_id', 'wallet_perfect'],
            'update' => ['email'],
        ];
    }
*/
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Login'),
            'account' => Yii::t('app', 'Account'),
            'email' => Yii::t('app', 'Email'),
            'hash' => Yii::t('app', 'Hash'),
            'auth' => Yii::t('app', 'Auth'),
            'code' => Yii::t('app', 'Code'),
            'referral_id' => Yii::t('app', 'Referral Login'),
            'forename' => Yii::t('app', 'First Name'),
            'surname' => Yii::t('app', 'Last Name'),
            'perfect_usd' => Yii::t('app', 'Perfect Money USD wallet'),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return static::hashAuthKey($this->auth);
    }

    public function validateAuthKey($authKey)
    {
        return static::hashAuthKey($this->auth) == $authKey;
    }

    protected static function hashAuthKey($key)
    {
        $ip = Yii::$app->request->getUserIP();
        if ($ip) {
            $key .= 'devil' . $ip;
        }
        return hash('sha512', $key);
    }

    public function generateAuthKey() {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode() {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public function setPassword($value) {
        if (!password_verify($value, $this->hash)) {
            $this->_password = $value;
            $this->hash = password_hash($value, PASSWORD_DEFAULT);
            $this->generateAuthKey();
        }
    }

    public function getPassword() {
        return $this->_password;
    }

    public function validatePassword($value) {
        return password_verify($value, $this->hash);
    }

    public function dumpErrors() {
        foreach($this->getErrors() as $name => $message) {
            Yii::$app->session->addFlash($name . ': ' . $message);
        }
    }

    public function sendEmail($params, $template = 'basic') {
        return God::mail($this->email, $template, $params);
    }

    public function getReferral() {
        return $this->hasOne(static::className(), ['id' => 'referral_id']);
    }

    public function walletAttributes() {
        return ['perfect_usd'];
    }
}
